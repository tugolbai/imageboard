import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TredData } from '../../models/tred.model';
import { TredsService } from '../../services/treds.service';

@Component({
  selector: 'app-edit-tred',
  templateUrl: './edit-tred.component.html',
  styleUrls: ['./edit-tred.component.sass']
})
export class EditTredComponent implements OnInit {
  @ViewChild('f') form!: NgForm;

  constructor(
    private tredsService: TredsService,
  ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    const tredData: TredData = this.form.value;
    this.tredsService.createTred(tredData).subscribe();
  }
}
