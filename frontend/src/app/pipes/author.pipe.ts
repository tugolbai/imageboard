import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'author'
})

export class AuthorPipe implements PipeTransform {
  transform(value: string, ...args: unknown[]): string {
    if (value) {
      return value;
    }

    return 'Anonymous';
  }
}
