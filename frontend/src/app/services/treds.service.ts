import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Tred, TredData } from '../models/tred.model';

@Injectable({
  providedIn: 'root'
})
export class TredsService {

  constructor(private http: HttpClient) { }

  getTreds() {
    return this.http.get<Tred[]>(environment.apiUrl + '/treds').pipe(
      map(response => {
        return response.map(productData => {
          return new Tred(
            productData.id,
            productData.author,
            productData.message,
            productData.image
          );
        });
      })
    )
  }

  createTred(tredData: TredData) {
    const formData = new FormData();

    Object.keys(tredData).forEach(key => {
      if (tredData[key] !== null) {
        formData.append(key, tredData[key]);
      }
    });

    return this.http.post(environment.apiUrl + '/treds', formData);
  }
}
