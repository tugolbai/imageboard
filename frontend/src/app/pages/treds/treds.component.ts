import { Component, OnInit } from '@angular/core';
import { Tred } from '../../models/tred.model';
import { TredsService } from '../../services/treds.service';

@Component({
  selector: 'app-treds',
  templateUrl: './treds.component.html',
  styleUrls: ['./treds.component.sass']
})
export class TredsComponent implements OnInit {
  treds: Tred[] = [];

  constructor(private tredsService: TredsService) { }

  ngOnInit(): void {
    this.tredsService.getTreds().subscribe({
      next: treds => {
        this.treds = treds;
      },
      error: err => {
        console.error(err)
      }
    });
  }

}
