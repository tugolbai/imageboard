export class Tred {
  constructor(
    public id: string,
    public author: string,
    public message: string,
    public image: string,

  ) {}
}

export interface TredData {
  [key: string]: any;
  author: string;
  message: string;
  image: File | null;
}
