const express = require('express');
const multer = require('multer')
const db = require('../fileDb');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', (req, res) => {
    const treds = db.getItems();
    return res.send(treds);
});


router.post('/', upload.single('image'), async (req, res, next) => {
    try {
        if (!req.body.message) {
            return res.status(400).send({message: 'Wrong data'});
        }

        const tred = {
            author: req.body.author,
            message: req.body.message,
        };

        if (req.file) {
            tred.image = req.file.filename;
        }

        await db.addItem(tred);

        return res.send({message: 'Created new tred', id: tred.id});
    } catch (e) {
        next(e);
    }
});

module.exports = router;